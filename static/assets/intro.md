Esta página se creó con el objetivo de registrar distintos experimentos sonoros que en [**Ayrsd**](https://gitlab.com/ayrsd) estamos haciendo en relación a [**AudioStellar**](https://gitlab.com/ayrsd/audiostellar).

La idea es documentar a modo de bitácora un proceso de búsqueda sonora. De esta manera, se describirá cierto material sonoro, sus parametros y mutaciones, adjuntando el material sonoro resultante.

Mucho del material es generado dentro de audiostellar y procesado posteriormente en Bitwig Studio.
