const choo = require('choo')
const devtools = require('choo-devtools')
const html = require('choo/html')
const raw = require('choo/html/raw')
const css = require('sheetify')
const marked = require('marked')
const fs = require('fs')

const introText = fs.readFileSync('static/assets/intro.md').toString()
const Entry = require('./Entry.js')
const entry = new Entry()
const entries = JSON.parse(fs.readFileSync('static/assets/entries.json'))

var app = choo()
app.use(devtools())

app.use(function (state, emitter) {
  state.title = 'Bitácora de AudioStellar'
})

var mainStyle = css`
  body {
    font-family:monospace;
    background:#1c1c1c;
    color:#f8f8f2;
    padding: 0% 5% 0% 5%;
    font-size:1.5rem;
  }

  a:active{
    color:#66d9ef;
  }

  a:visited{
    color:#9abf51;
  }

  .title{
    position:fixed;
    min-width:90%;
    background:#1c1c1c;
  }

  .content{
    padding-top:120px;
  }
`

app.route('/', mainView)

function mainView (state, emit) {
  return html`
  <body class=${mainStyle}>
    <div class="title">
      <h1> ${state.title} </h1>
      <hr>
    </div>
    <div class="content">
      <div class="intro">
      ${raw(marked(introText))}
      </div>
      ${entriesView()}
    </div>
  </body>
  `
}

function entriesView (state, emit) {
  return html`
   <div>
      ${entries.map(buildEntries)}
   </div>
  `

  function buildEntries (obj) {
    return entry.render(obj)
  }
}

app.mount('body')
