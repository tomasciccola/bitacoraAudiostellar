#!/usr/bin/node

// Script para generar una nueva entrada en la bitácora
const fs = require('fs')
const colors = require('colors')
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

const audioFile = process.argv[2]

var entries = JSON.parse(fs.readFileSync('static/assets/entries.json'))
var entry = {
  date: new Date().toISOString().slice(0, 10),
  description: '',
  analisis: '',
  sound: ''
}

entries.map(function (e) {
  if (e.date === entry.date) {
    console.log('ya hiciste una entrada a la bitácora hoy uatch, guardatela para otro día. Cerrando...')
    process.exit(1)
  }
})

console.log('Nueva entrada de AudioStellar:\n\n\n'.blue)
console.log('Con fecha: '.yellow)
console.log(String(entry.date).magenta)

var descriptionEntry = 'Tipeate una descripción del sonido que grabaste;Principalmente: dataset, parámetros, extra info:\n\n'
var analisisEntry = 'Y que sentis con lo que grabaste?: \n\n'

readline.question(descriptionEntry, getDescription)

function getDescription (desc) {
  entry.description = desc
  readline.question(analisisEntry, getAnalisis)
}

function getAnalisis (a) {
  var soundFilename = 'assets/audio/' + entry.date + '.wav'
  entry.analisis = a
  entry.sound += soundFilename
  entries.push(entry)
  fs.writeFileSync('static/assets/entries.json', JSON.stringify(entries))

  fs.createReadStream(audioFile)
    .pipe(fs.createWriteStream(soundFilename))
    .on('close', () => process.exit(1))
}
