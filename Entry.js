const Nanocomponent = require('nanocomponent')
const html = require('choo/html')
const css = require('sheetify')

class Entry extends Nanocomponent {
  constructor () {
    super()

    this.data = {
      date: '',
      description: '',
      analisis: '',
      sound: '',
      gif: ''
    }

    this.entryStyle = css`
      :host {
        display:grid;
        grid-template-columns: 50% 50%;
        margin-top: 4%;
        border-top: 1px solid white;
        align-items:center;
      }

      :host >  h2{
        color:#f92672;
      }

      :host controls{
        background:#1c1c1c;
      }
    `
  }

  createElement (d) {
    this.data = d

    return html`
      <div class=${this.entryStyle}>
        <h2> Entrada: ${d.date} </h2>
        <p> <b>Parámetros: ${d.description} </b></p>
        <audio controls>
          <source src=${d.sound} type="audio/mpeg">
        </audio>
        <p> Detalle: ${d.analisis} </p>
      </div>
    `
  }
}

module.exports = Entry
