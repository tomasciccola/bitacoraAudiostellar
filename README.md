# AUDIO STELLAR --> BITÁCORA

La idea es ir subiendo audios + un texto descriptivo de la búsqueda y características del sonido (parámetro/modo/etc) y tal vez un gif que muestre un toque el movimiento (concreto o aprox)

schema:

{
  sonido: WebAudioFile,
  desc: String,
  gif: Gif
}
